<?php

require '../../config.php';
require_login();
$PAGE->set_title("ForceFeedback");
$PAGE->set_heading("Оцінка навчального курсу");
$PAGE->set_url($CFG->wwwroot . '/local/forcefeedback/index.php');

echo $OUTPUT->header();
?>
<link rel="stylesheet" type="text/css" href="style.css">

<?php
echo $OUTPUT->notification("Шановний студенте, увага!");
echo "<h5>Перш ніж продовжити роботу на Навчальному Порталі, просимо Вас пройти швидке оцінювання курсів, що вже завершились.</h5>";
echo "<p><font size='2px'><i>*В середньому час на проходження оцінювання складає 1-2 хвилини. </i></font></p><hr>";

$a = get_course_feedbacks_completion();

foreach ($a as $feedbackModule) {
  echo "<div class='feedbackRender'><div class='contentDiv'><i class='fa fa-graduation-cap iconFeedback'></i></div><div class='contentDiv'><h4> Курс: '" .
    $feedbackModule->coursename .
    "'</h4>";
  $courseTeachers = get_course_teachers($feedbackModule->course);
  if ($courseTeachers) {
    if (sizeof($courseTeachers) == 1) {
      echo "<p><strong>Викладач:</strong> " .
        reset($courseTeachers)->lastname .
        " " .
        reset($courseTeachers)->firstname .
        "</p>";
    } else {
      echo "<p><strong> Викладачі:</strong> ";
      foreach ($courseTeachers as $teacher) {
        $teachers =
          $teachers . $teacher->lastname . " " . $teacher->firstname . ", ";
      }
      echo rtrim($teachers, ', ') . "</p>";
    }
  }
  echo "<img src ='" .
    $CFG->wwwroot .
    "/mod/feedback/pix/icon.svg' alt='feedback pix'>  <a href='" .
    $CFG->wwwroot .
    "/mod/feedback/view.php?id=" .
    $feedbackModule->moduleid .
    "'>" .
    $feedbackModule->feedback_name .
    "</a>";
  echo "</div></div><br>";
}
echo "<br><br>";
echo "<br><br>";

echo $OUTPUT->footer();


?>
