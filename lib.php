<?php

require_once $CFG->dirroot . '/local/forcefeedback/locallib.php';

function local_forcefeedback_after_require_login()
{
  global $ME;
  if (!is_siteadmin() && !isguestuser() && !WS_SERVER) {
    if ($_SERVER[REQUEST_URI] !== '/local/forcefeedback/index.php') {
      global $DB, $USER, $PAGE;
      $userid = $USER->id;
      $feedback_completions = $DB->get_field_sql(
        "select count(id) not_completed from mdl_feedback where course in (select id from mdl_course where id in (select instanceid from  mdl_role_assignments join mdl_context on mdl_role_assignments.contextid=mdl_context.id where roleid=5 and userid= :user_id) and visible=1) and name='Оцініть, будь ласка, курс' and id not in (select feedback from mdl_feedback_completed where userid= :user_id2) and id in (select instance from mdl_course_modules where module in (select id from mdl_modules where name='feedback') and visible=1 and completionexpected<>0 and completionexpected< :time)",
        array('user_id' => $userid, 'user_id2' => $userid, 'time' => time())
      );
      if (
        $feedback_completions &&
        strpos($_SERVER[REQUEST_URI], 'mod/feedback') < 1
      ) {
        redirect(new moodle_url('/local/forcefeedback/index.php'));
      }
    }
  }
}
?>
