<?php
defined('MOODLE_INTERNAL') || die();

require_once $CFG->dirroot . '/lib/completionlib.php';

function get_course_feedbacks_completion()
{
  global $DB, $USER;
  $userid = $USER->id;
  $feedback_completions = $DB->get_records_sql(
    "select mdl_feedback.id as feedbackID,mdl_course_modules.id as moduleID, mdl_course_modules.completionexpected as completionexpected, mdl_feedback.course, mdl_course.fullname as courseName, mdl_feedback.name as feedback_name from mdl_feedback inner join mdl_course_modules on mdl_feedback.id=mdl_course_modules.instance inner join mdl_course on mdl_feedback.course=mdl_course.id where mdl_feedback.course in (select mdl_course.id from mdl_course where mdl_course.id in (select instanceid from  mdl_role_assignments join mdl_context on mdl_role_assignments.contextid=mdl_context.id where roleid=5 and userid= :user_id) and visible=1) and name='Оцініть, будь ласка, курс' and mdl_feedback.id not in (select feedback from mdl_feedback_completed where userid= :user_id2) and mdl_feedback.id in (select instance from mdl_course_modules where module in (select mdl_modules.id from mdl_modules where name='feedback') and visible=1 and completionexpected<>0 and completionexpected< :time) and mdl_course_modules.module in (select mdl_modules.id from mdl_modules where name='feedback')",
    array('user_id' => $userid, 'user_id2' => $userid, 'time' => time())
  );

  return $feedback_completions;
}

function get_course_teachers($courseid)
{
  global $DB, $USER;
  $userid = $USER->id;
  $role = $DB->get_record('role', array('shortname' => 'editingteacher'));
  $context = get_context_instance(CONTEXT_COURSE, $courseid);
  $teachers = get_role_users($role->id, $context);
  return $teachers;
}
